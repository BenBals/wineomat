module Main exposing (..)

import Css exposing (..)
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css, href, src, rel)
import Html.Styled.Events exposing (onClick)
import Browser
import Css.Global

import Helpers
import Questions
import Styles


main =
  Browser.sandbox { init = init, update = update, view = view >> toUnstyled }


-- MODEL

type AppState = Welcome | Started

type alias Model =
    { appState : AppState
    , unansweredQuestions : List Questions.Question
    , answeredQuestions : List Questions.Question
    , factors : Questions.Factors
    }


init : Model
init =
    { unansweredQuestions = Questions.questions
    , answeredQuestions = []
    , appState = Welcome
    , factors =
        { sweetness = 0
        , acidity   = 0
        , tannin    = 0
        }
    }

-- UPDATE

type Msg = Start | Answer Questions.Factors | Reset

update : Msg -> Model -> Model
update msg model =
    case msg of
        Start -> { model | appState = Started }
        Answer factors ->
          case List.head model.unansweredQuestions of
            Nothing -> model
            Just q ->
              { model |
                    unansweredQuestions = Helpers.tail model.unansweredQuestions
                    , answeredQuestions = q :: model.answeredQuestions
                    , factors = Questions.updateFactors model.factors factors
              }
        Reset -> init

-- VIEW

view : Model -> Html Msg
view model =
    let inner = case model.appState of
            Welcome -> welcomeView
            Started ->
                case model.unansweredQuestions of
                    [] -> resultView <| Questions.calculateBestWine model.factors
                    currentQuestion :: rest ->
                        questionView currentQuestion
   in div [ css [ Styles.wrapper ] ]
       [ inner
       , Css.Global.global Styles.global
       , node "link" [ href "https://fonts.googleapis.com/css?family=Playfair+Display:400,900i", rel "stylesheet" ] []
       ]

welcomeView : Html Msg
welcomeView =
    div []
        [ h1 [ css [Styles.heading] ] [ text "Wine-O-Mat" ]
        , h2 [ css [Styles.textCenter] ] [ text "The interactive wine tasting!"]
        , div [ css [Styles.centerElement] ]
            [ Styles.btn [ onClick Start ] [ text "Start!" ]
            ]
        ]

questionView : Questions.Question -> Html Msg
questionView (Questions.Question title answers) =
    div []
        [ h1 [ css [Styles.heading] ] [ text title ]
        , div [ css [Styles.centerElement] ] (List.map answerButton answers)
        ]

resultView : Questions.Wine -> Html Msg
resultView wine =
    div []
        [ h2 [ css [Styles.textCenter] ] [ text "The results are in!" ]
        , p  [ css [Styles.textCenter] ] [ text "The best wine for you is..."]
        , h1 [ css [Styles.heading] ] [ text <| "Our " ++ wine.name ++ "!"]
         , Styles.btnSmall [ onClick Reset ] [ text "Do it again!" ]
        ]

answerButton : (String, Questions.Factors) -> Html Msg
answerButton (answerText, factors) = Styles.btn [ onClick <| Answer factors ] [ text answerText ]

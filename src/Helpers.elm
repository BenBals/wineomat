module Helpers exposing (..)

tail xs = List.tail xs |> Maybe.withDefault []

module Questions exposing (..)

type WineNames = Ortega | Nebbiolo | PapeClement

type alias Factors =
    { sweetness : Int
    , acidity : Int
    , tannin : Int
    }

type Question = Question String (List (String, Factors))

type alias Wine =
    { name : String
    , sweetness : Int
    , acidity : Int
    , tannin : Int
    }

-- LOGIC
calculateBestWine : Factors -> Wine
calculateBestWine factors =
    if wines == []
    then ortega
    else
      let ratedWines = List.map (rateWine factors) wines
      in List.foldr (\(w1,s1) (w2,s2) ->
                        if s1 > s2
                        then (w2, s2)
                        else (w1, s1)
                    ) (ortega, 10000000) ratedWines
          |> Tuple.first

-- lower is beter
rateWine : Factors -> Wine -> (Wine, Int)
rateWine factors wine =
    ( wine
    , (abs (wine.sweetness - factors.sweetness))
    + (abs (wine.tannin    - factors.tannin))
    + (abs (wine.acidity   - factors.acidity))
    )

updateFactors : Factors -> Factors -> Factors
updateFactors f1 f2 =
    { sweetness = f1.sweetness + f2.sweetness
    , acidity = f1.acidity + f2.acidity
    , tannin = f1.tannin + f2.tannin
    }


-- ++++
-- DATA
-- ++++
idFactors =
    { sweetness = 0
    , acidity = 0
    , tannin = 0
    }


-- WINES

wines = [ortega, nebbilio, papeClement]

ortega =
    { name = "Ortega"
    , sweetness = 100
    , acidity = 20
    , tannin = 52
    }

nebbilio =
    { name = "Nebbilio"
    , sweetness = 20
    , acidity = 47
    , tannin = 44
    }

papeClement =
    { name = "Pape Clement"
    , sweetness = 27
    , acidity =  72
    , tannin = 10
    }


-- QUESTIONS

questions =
    [ sweetness
    , countries
    , firstTasteInfo
    , firstTasteRating
    , secondTasteInfo
    , secondTasteRating
    ]

sweetness =
    Question
        "Do you like sweet wines?"
         [ ( "Yes"
           , { sweetness = 20
             , acidity = 0
             , tannin = 0
             })
         , ( "No"
           , { sweetness = -30
             , acidity = 20
             , tannin = 10
             })
         ]

countries =
    Question
        "Which country makes your favorite wine?"
         [ ( "France"
           , { sweetness = 10
             , acidity = 10
             , tannin = -10
             })
         , ( "Britain"
           , { sweetness = -30
             , acidity = 0
             , tannin = 10
             })
          , ( "Italy"
           , { sweetness = -30
             , acidity = 0
             , tannin = 10
             })
          , ( "America"
           , { sweetness = -30
             , acidity = -30
             , tannin = -30
             })
          , ( "other"
           , { sweetness = 0
             , acidity = 0
             , tannin = 0
             })
         ]

firstTasteInfo =
    Question
        "Please go to the counter and ask to taste Mystery Wine One."
        [ ("Done", idFactors) ]

firstTasteRating =
    Question
        "How did you like the wine?"
         [ ( "Perfect!"
           , { sweetness = 30
             , acidity = 20
             , tannin = 0
             })
         , ( "To sweet!"
           , { sweetness = -30
             , acidity = 0
             , tannin = 10
             })
          , ( "Meh, so sour."
           , { sweetness = 10
             , acidity = -20
             , tannin = 10
             })
          , ( "To earth for me."
           , { sweetness = 10
             , acidity = 0
             , tannin = -30
             })
          , ( "Hated it."
           , { sweetness = -30
             , acidity = -20
             , tannin = 0
             })
         ]


secondTasteInfo =
    Question
        "Go and do our second tasting."
        [ ("Done", idFactors) ]

secondTasteRating =
    Question
        "How did you like the wine?"
         [ ( "Very balanced!"
           , { sweetness = -10
             , acidity = 20
             , tannin = 40
             })
         , ( "Not enough body!"
           , { sweetness = 0
             , acidity = -10
             , tannin = 20
             })
          , ( "Didn't like the bouquet."
           , { sweetness = 10
             , acidity = 20
             , tannin = 10
             })
          , ( "Corked!"
           , { sweetness = 10
             , acidity = 0
             , tannin = 0
             })
          , ( "Too dry."
           , { sweetness = -20
             , acidity = 20
             , tannin = 10
             })
         ]


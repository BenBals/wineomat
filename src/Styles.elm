module Styles exposing (..)

import Css exposing (..)
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css, href, src)
import Html.Styled.Events exposing (onClick)
import Css.Global as Global

wrapper =
    Css.batch
        [ color (hex "fff")
        ]

heading =
    Css.batch
        [ fontSize (rem 3)
        , textAlign center
        , fontFamilies [qt "Playfair Display", .value serif]
        , fontWeight (int 900)
        , fontStyle italic
        ]

textCenter =
    Css.batch
        [ textAlign center
        ]

centerElement =
    Css.batch
        [ position relative
        , transform (translate (pct -50))
        , left (pct 50)
        , display inlineBlock
        ]

btn =
    styled button
        [ border3 (px 1) solid (hex "fff")
        , color (hex "fff")
        , backgroundColor transparent
        , padding (px 10)
        , fontSize (rem 1)
        , cursor pointer
        ]

btnSmall =
    styled button
        [ color (hex "fff")
        , cursor pointer
        , backgroundColor transparent
        , border (px 0)
        , textDecoration underline
        ]



global =
    [ Global.selector "body"
          [ padding (px 0)
          , backgroundColor (hex "8e0e00")
          , property "background" "-webkit-linear-gradient(to right, #8e0e00, #1f1c18);"
          , property "background" "linear-gradient(to right, #8e0e00, #1f1c18);"
          ]
    ]
